#!/usr/bin/make -f

VERSION = 0.10

texfiles=$(wildcard *.tex)
pdffiles=$(patsubst %.tex,%.pdf,$(texfiles))
idxfiles=$(patsubst %.tex,%.idx,$(texfiles))
dvifiles=$(patsubst %.tex,%.dvi,$(texfiles))
psfiles=$(patsubst %.dvi,%.ps,$(dvifiles))

DISTFILES = Makefile $(texfiles) $(pdffiles)

all: $(dvifiles) $(psfiles) $(pdffiles)

%.pdf: %.tex
	latex $<
	if [ -f $(patsubst %.tex,%.idx,$<) ] ; then \
		makeindex $(patsubst %.tex,%.idx,$<) ; \
	fi
	pdflatex $<

%.dvi: %.tex
	latex $<
	if [ -f $(patsubst %.tex,%.idx,$<) ] ; then \
		makeindex $(patsubst %.tex,%.idx,$<) ; \
	fi
	latex $<

%.ps: %.dvi
	dvips $<

clean:
	-rm *.out *.log *.aux *.toc $(pdffiles) $(idxfiles) $(dvifiles) $(psfiles)
	-rm *.ind *.idx *.ilg
	-rm -rf domino-knights-$(VERSION)
	-rm -f domino-knights-$(VERSION).tar.gz

dist: $(pdffiles)
	-mkdir -p domino-knights-$(VERSION)
	-for i in $(DISTFILES) ; do ln -f $$i domino-knights-$(VERSION) ; done 
	-tar -czvf domino-knights-$(VERSION).tar.gz domino-knights-$(VERSION)


.PHONY: all clean dist
