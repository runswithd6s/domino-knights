\documentclass{article}
\usepackage[colorlinks]{hyperref}
\usepackage{latexsym}
\usepackage{makeidx} % for indexing

% Double-spaced for editing
%\linespread{1.6}

\title{Domino Knights, v0.12}
\author{Chad C. Walstrom}

\begin{document}
\maketitle

\begin{abstract}
Domino Knights is an alternative to the numerous battle-focused,
collectable card games on the market today.  Instead of special cards,
it uses games that you may already have in your bookshelf: a set of
dominos, a deck of cards, and pencil and paper.  The concept of the
game is simply to defeat your opponents in battle using Knights you've
trained from the Peasants of the countryside. This game is designed
for two to six players.
\end{abstract}

% Insert abstract here

\newpage
Copyright \copyright 2015 Chad C. Walstrom

\par This work is provided to you under the Creative Commons
Attribution 4.0 International license.  For the full text of this
license, see the official website at:

\begin{quote}
\href{http://creativecommons.org/licenses/by/4.0/legalcode}{http://creativecommons.org/licenses/by/4.0/legalcode}
\end{quote}

\par {\em You are free to:}
\begin{description}
\item [Share]: copy and redistribute the material in any medium or format
\item [Adapt]: remix, transform, and build upon the material
\end{description}

\par for any purpose, even commercially. The licensor cannot revoke these
freedoms as long as you follow the license terms.

\par {\em Terms:}
\begin{description}
\item [Attribution]: You must give appropriate credit, provide a link
to the license, and indicate if changes were made. You may do so in
any reasonable manner, but not in any way that suggests the licensor
endorses you or your use.
\item [No additional restrictions] You may not apply legal terms or
technological measures that legally restrict others from doing
anything the license permits.
\end{description}

\par {\em Notices:}
\par You do not have to comply with the license for elements of the
material in the public domain or where your use is permitted by an
applicable exception or limitation.

\par No warranties are given. The license may not give you all of the
permissions necessary for your intended use. For example, other rights
such as publicity, privacy, or moral rights may limit how you use the
material.

% Insert trademark attributes here

%\section{Acknowledgements}
% Go to the front of the article?

\newpage
\tableofcontents

\newpage
\section{Introduction}
Domino Knights is an alternative to the numerous battle-focused,
collectable card games on the market today.  Instead of special cards,
it uses games that you may already have in your bookshelf: a set of
dominos, a deck of cards, and pencil and paper.  The concept of the
game is simply to defeat your opponents in battle using Knights you've
trained from the Peasants of the countryside. This game is designed
for two to six players.

Dominos are used to represent the Peasants a player recruits and
trains into Knights with which she battles opposing Kingdoms.  One
side of the domino represents an attack power, and the other side
represents a defend power.  Which side is use for attack or defend is
determined by how the player brings a Knight in to play.

The standard deck of cards provides randomness and variety to the game
by determining the type of Knight you bring in to play, and by
providing special abilities to those Knights.  It is recommended that
you use one standard deck with the Jokers for every three players.

The pencil and paper is used to track a player's Kingdom Health points,
which start out at $3\times \textbf{maximum pip value}$ of the domino
set.  For example, if the maximum pip value for the domino set is 9,
each player's Kingdom Health Points starts out at $3\times{}9=27$.  When
a player is reduced to zero Kingdom Health Points, that player is no
longer able to successfully maintain an army; she is ejected from the
game.  The game is won by the ``last player standing,'' the last
player with Kingdom Health Points.

\section{Terminology}
Every new game comes with new terminology to learn, and this one is no
exception.  The following list describes the most important terms to
know for this game.  For the sake of examples throughout this article,
four players will be used: Alice, Bob, Charlie, and Dianne.

\begin{description}
\item [Attack]: third phase of a player's turn.
\item [Battlefield]: not an actual area on the table, merely a term
referring to the Knights involved in a single battle
\item [Candidate]: a pips-down domino lying on face-down training
cards in front of the player.  Promoted to a Knight during the
player's next Promote phase.
\item [Cleanup]: fourth phase of a player's turn
\item [Deck]: face-down pile of cards in the center of the table
\item [Discard]: fifth phase of a player's turn.  Also refers to the
act of placing an unwanted card face-up on the discard pile.
\item [Discard Pile]: face-up pile of cards next to the deck.  These
cards can not be used until the deck is reshuffled.
\item [Draw]: first phase of a player's turn.  Refers both to drawing
Peasants from the countryside and drawing cards from the deck.  Refers
to an attack resolution where neither Knight defeats the other, "a
draw".
\item [Kingdom]: Synonymous with Player
\item [Kingdom Health Point]: tracks the relative health of a Kingdom
\item [Knight]: a pips-up domino lying on face-up training cards in
front of the player.  Used to attack or defend Kingdoms.
\item [Graveyard]: a pips-up stack of defeated Knights located in
front of each player
\item [Peasant]: a pips-down domino in the center of the table.  Draw
Recruits from this pool at the beginning of the Draw phase.
\item [Pip]: a the dot on a domino.
\item [Promote]: second phase of a player's turn.  Promote Candidates
to Knights.
\item [Recruit]: a pips-down domino in front of the player.  Use
training cards to promote to a Candidate.
\item [Round]: the period in which all players have taken a turn.
\item [Special Card]: a card that can be used at any point in the game
to influence Peasants, Recruits, Candidates, or Knights.
\item [Train]: second phase of a player's turn
\item [Training Card]: the face value of the cards from 2 to King used
in sum to train Candidates.
\item [Training Cost]: equal to the sum of the pips on a domino
\end{description}

\section{Setup}
Game play begins with Players naming their respective Kingdoms.  Have
fun with it!  Next, determine the goal of the game, whether it is
``last Kingdom standing'' or ``Healthiest Kingdom after $T$ minutes.''
See the section on ``Winning the Game'' for more details.

The first Kingdom to start the game is determined by a random card
draw.  The player with the highest card value goes first, with Kings
being highest and Aces lowest.  If there is a tie between the highest
drawn players, draw again until a winner is determined.  The cards are
placed back in the deck and reshuffled.  Five cards are dealt to each
player, who may look at their cards at any time.

Dominos are shuffled, faced-down in the center of the table.  These
dominos are known as \emph{Peasants}.  Each player draws two Peasants
and places them pips-down in their pool of Recruits.  As Knights are
defeated in battle, they are placed pips-up in a player's graveyard
stack.  If there is ever a case where the Peasants are exhausted,
players return all but the top domino of their graveyard stack to the
Peasants pool.

The deck of cards is placed face down in the center of the table.  The
first player draws one card from the deck.

\section{Game Play}
Each player's turn consists of the following steps, starting with the
player who won the draw and progressing clockwise around the table:
{\bf Draw, Promote, Train, Attack, Discard}.

\subsection{Draw}
Replenish the pool of Recruits to two dominos by drawing from the
Peasants in the center of the table.  The player may look at the
Recruits at any time.  When there is less than five Peasants left,
recycle the player's graveyards as described in the Setup.

Next, each player must have six cards at the beginning of her turn.
Draw as many cards from the deck to reach six cards.

\subsection{Promote}
Promote Candidates into Knights by flipping the training cards face-up
and placing the Candidate pips-up upon the cards.  Arrange the Knight
so that the desired attack power (pips) faces the center of the table
and the defense power faces the player.  Discard any Special Cards
that were only active for the last turn.

Having the Promote phase following the Draw phase allows the player to
use the knowledge about what type of Recruits can be trained that
round to influence her decision on how to bring her current Candidates
into play as Knights.  For example,

\begin{quote}
{\em Alice has a 9/2 Candidate that she can either bring in to play
as a 9/2 Knight or a 2/9 Knight.  She draws two Peasants as Recruits,
a 5/2 and a 5/7.  She has enough points in training cards to train
both the Recruits, but she has only one Knight in play at the
moment, a 2/5.  Because she is very low on attack power, and because
she knows she'll have a more balanced set of Knights next turn, she
opts to bring the Candidate in as a 9/2 Knight.}
\end{quote}

\subsection{Train}
The Training Cost of a Recruit is equal to the sum of its pips.  The
cost may be covered using the face value of cards of the \emph{same
suit} that equal or exceed the training cost.  Jacks count as 11
points, Queens as 12, and Kings as 13.  If Tens, Jacks, Queens, and
Kings are used as Training Cards, they lose their abilities as
Special Cards.

To train a Recruit, a player must fulfill its training cost in
Training Cards.  Stack the training cards face down in front of the
player, and place the Recruit ``pips-down'' upon the training cards.
It is now a Candidate in training.

\subsubsection{Optional Rule: Multi-turn Training}
{\em by Gabe Turner}

A player can begin to train a Recruit even if the training cost is not
met that turn.  Place the training cards face down and the Recruit
pips-down as per normal training rules. The player may then add
training cards to the Candidate each turn until the cost is met.  The
Candidate may then be promoted to a Knight in the Promote phase of
the \emph{following} turn.

\subsubsection{Optional Rule: Train after Discard}
{\em by Gabe Turner}

In the interest of speeding up game play, move the Training phase
after the Discard phase of a player's turn.  The next player's turn
begins, and the previous player has until her next Draw phase to train
Recruits.

If the the A$\diamondsuit$ -- Wealth\footnote{See ``The Aces: Instant
Gratification'' for more details on Wealth and other Special Cards.}
-- is played, it may be disrupted at any time up until the Promote
phase of the player's next turn.

\subsubsection{Optional Rule: Different Suits}
This rule drops the requirement that training cards must be of the
same suit, instead allow any combination of suits to be used.  All
suits must be displayed for the Knight.  This Knight may be affected
by a broader range of special effects cards, with beneficial and
detrimental effects.  For example,

\begin{quote}
{\em Charlie has a 4/9 Knight that was trained with a 5$\heartsuit$
and a 9$\spadesuit$.  His turn had passed and now Dianne is attacking
him in retribution to his earlier attack.  Dianne has committed two
attackers, assuming he only had one blocker left.  Charlie plays a
K$\heartsuit$, and is allowed to re-use his 4/9 Knight for defense,
which he places in front of Dianne's all-$\spadesuit$ attacker.
Undeterred, Dianne plays a Q$\spadesuit$, making his 4/9
$\spadesuit\/\heartsuit$ Knight unable to block her attacker.}
\end{quote}

\subsection{Attack}
The ultimate objective of any attack is to reduce the opponent's
Kingdom Health Points.  The opponent's objective is to prevent that
from happening.  Damage for an attack is either taken by the defending
Knights or the Kingdom, but not both.  For example, a defending Knight
without a single pip in defense -- a half-blank domino brought in to
play with the pips used for attack power -- can block an exceedingly
powerful attack, sacrificing itself to protect the Kingdom from
damage. An attacking player's objective is to overwhelm the opponent
with numbers so that a free Knight can get through the defenses and
damage the Kingdom itself.

A player may attack any other player, and she may attack any number of
players during her turn.  However, each attack must be declared and
resolved separately.  Additionally, each player may be attacked only
once per turn.  For example,

\begin{quote}
{\em Alice may attack Bob, resolve, and then attack Charlie.  Alice
may not attack Bob or Charlie again until her next turn, rather she
may only attack Dianne.}
\end{quote}

The attack sequence is as follows: {\bf Declare and Commit to the
Attack, Aid the Attack, Declare and Commit the Defense, Aid the
Defense, Resolve, Clean-up.}


\subsubsection{Declare and Commit to the Attack}
The attacking player must first declare who she is attacking, then
declare which Knights she is committing to the attack.  Knights attack
the Kingdom as individual fighters.

\subsubsection{Aid in the Attack}
The attacking player may ask for aid at this time.  Aid is provided by
any other player by committing Knights to attack the defending
\emph{player}. Aid can be given even if it isn't explicitly asked for,
but must be declared before the defending player begins committing
Knights to defense.

\subsubsection{Optional Rule: Attack as a Unit}
The attacking player may send multiple Knights into battle as a single
unit.  The attack power and defense power of the unit is the sum of
each member of the unit.  To defeat this unit, the defending Knight or
Knights must defeat the combined defense of all the Knights in the
unit.\footnote{This option was not allowed in the original game for
fear of it overbalancing power to the attackers.  Early versions also
allowed unblocked damage to be taken from the Kingdom Health Points;
this is no longer the case. It is only fair that if Knights can defend
as units, then they should also be able to attack as units.}

Aid can also be given as Knights forming units with the other
attacking Knights or as individual attackers.

\subsubsection{Declare and Commit to the Defense}
The defending player may then choose whether or not to defend.  If she
does defend, she does so by indicating which of her Knights will
defend against each attacking Knight.  For example, by placing the
defending Knight directly in front of the attacking Knight.  A Knight
that has already attacked or defended before this Attack sequence
began may not be used to defend.

Multiple defending Knights may commit to a single attacking Knight.
For both attack and defense power, treat the defending Knights as if
it were a single Knight, a unit.  In order for the attacking Knight to
defeat either Knight, it must defeat the unit as a whole.

\subsubsection{Aid in the Defense}
As with the attacking player, the defending player may ask for aid.
Defense aid can be given by other players committing their Knights to
a player's defense.  Aid may be given, even if the defending player
has decided not to commit her own Knights in defense.

There are no rule-based allegiances, and any player may choose to
interfere by committing their own Knights to attack or defend
Kingdoms.  Knights providing aid in defense are allowed to act as a
unit.  To defeat any defensive Knight in a unit, all Knights in the
unit must be defeated.

\subsubsection{Resolution}
All attacks happen simultaneously.  To resolve which Knights are
victorious, compare the attack power of a Knight to the defense power
of the opposing Knight or unit.  If the attack is greater than the
combined defense, the attack succeeds.  If neither attack succeeds, it
is considered a draw; neither Knight is defeated.   If both attacks
succeed, both Knights are defeated.  All Knights involved remain
inactive for the remainder of each player's turn.

If an attack cannot be defended or blocked, its attack power reduces
the defending player's Kingdom's Health Points.  If the player has no
remaining Kingdom Health Points, she is ejected from the game.
Discard all cards and place all Knights, Candidates, and Recruits in
the player's graveyard.

\subsubsection{Clean-up}
After resolution is complete, send any defeated Knights to the
controlling player's graveyard stack and discard the Knights' training
cards.  Discard any special cards no longer in play.  Move any Knights
that survived the battle back in front of their respective players.

At this point, a player may choose to Attack another or decide to
Discard and end her turn.

\subsubsection{Optional Rule: Loot the Battfield}
The defeated Kingdom's land is absorbed by the opponent, and the
\emph{battlefield} may be looted.  Any single Knight defeated upon the
battlefield can be placed on the top of the victorious player's own
graveyard stack.  This option counters a game strategy where the
defeated player buries stronger Knights below weaker Knights in the
graveyard stack, making them unavailable for the Necromancer to raise
from the dead.

\subsection{Discard}
A player may not have more than five cards in her hand at the end of
her turn, but may discard any number of cards if she so decides.
Discard any extra cards face up on the discard pile.

\section{Special Cards}
What fun would any game be without special effects and attack cards?
This section details how Aces, the Face Cards, Jokers, and Tens can
affect game play.  Each of these cards can be played as \emph{instant
effect cards}; they can be played at any time.  Any number of face
cards can be played at once, by any player, and effects can be
stacked.

Although special cards may be used for points to train Recruits, they
lose their abilities as a special card.  When the last card of the
deck is drawn, the discard pile is reshuffled and placed as the deck.

The effects of special cards may ``stack'' upon each other.  The order
of precedence for the stacking effect is determined by the order in
which they were played.  If playing with multiple decks, identical
cards may also stack upon each other.

The longest period of time any special card can remain in effect is
one round.  With the exception of the Wealth card, this means that a
Special Card will remain in effect from the time it was played until
the player's next Draw phase.

\subsection{The Aces: Instant Gratification}
The Aces are the most powerful cards in the deck and can affect any
Knight on the board, not simply the player who uses the card.

\begin{itemize}
\item {A$\spadesuit$}: \emph{(Instant)} Death.  This card instantly
kills any domino on the board, whether it is ``pips-up'' or
``pips-down'' (blind kill).  Discard after killing the target Knight.
\item {A$\heartsuit$}: \emph{(Instant)} Life.  This trains a single
Knight from the Recruit pool directly into action with a single
training card (to indicate suit of the Knight).  The Knight may attack
or defend an unlimited number of times until the player's next Draw
phase. Discard this card then, or immediately after the Knight is
defeated, whichever comes first.
\item {A$\diamondsuit$}: \emph{(Instant)} Wealth.  Double the value of
all training cards in the player's hand during the Training step of a
player's turn.  Place the card face up on top of or in front
Candidates in training.  Discard during the Promote phase of the
player's next turn.
\item {A$\clubsuit$}: \emph{(Instant)} Power.  This card doubles the
attack and defend values for a single Knight in play.  Discard during
the Draw phase of the player's next turn or immediately after the
Knight is defeated, whichever comes first.
\end{itemize}

The Death card can be played any time on any Knight in play or being
trained.  This may be useful in preventing a player from building
Knights in her next turn.

The Life card is like having a surprise attacker or defender.  The
only requirement is that a single training card, regardless of value,
must be used to indicate the suit of the Knight.  It is most
impressive when using it to pull a Knight directly from the Recruit
pool into play.  If used in concert with a Joker (Necromancer), the
Knight can be stolen from any players grave and brought directly in to
play.  It can also be used on Knights already in play, even those that
have already attacked or defended during the round.

The Wealth \index{Wealth} card may not seem powerful at first, but
some Knights require many points to train.  The double nine domino,
for example requires 18 points in training cards of the same suit to
train.  When you play the Wealth card, place it face up either on top
of or directly in front of your Candidates.  This card can only be
disrupted during the Training phase of the player's turn.  Once the
Training phase has ended and the player has entered Attack or Discard
phases, the card is inactive and only remains in front of the player
to verify that training costs have been met during the Draw phase of
her next turn.

The probability for each of these cards to come into play is $1/54$.

\subsection{The Faces: Control Cards}
The Face Cards can be played on behalf of any player at the table and
remain in effect until the Draw phase of that player's next turn.
These cards should be placed face up next to the player affected.

\begin{itemize}
\item \textbf{King}: \emph{(Instant)} Magesty.  Knights of the King's
suit under the player's control may attack or defend an unlimited
number of times until the player's next Draw phase.
\item \textbf{Queen}: \emph{(Instant)} Loyalty.  Knights of the
Queen's suit may not attack the player who this card effects or
Knights the player controls that are of the same suit.  Her Knights,
in turn, also refuse to fight opponents of the same suit.
\item \textbf{Jack}: \emph{(Instant)} Courage.  Knights of the Jack's
suit gain Attack and Defense Bonuses of $1/3$ of maximum pip value for
the domino set.
\end{itemize}

The King is a figure of authority and honor, inspiring his Knights to
press on even under the grueling conditions of battle.  When the King
is in play, Knights of the same suit under the player's control may
defend {\em and attack} an unlimited number of times.  \footnote{This
ability is powerful, and if played correctly can be quite devastating
to an opponent.  One suggestion for balancing this out is to force
Knights that attack or defend more than once must be inactive for one
round of play starting with the Draw phase of the player's next turn.}

The Queen is the love of her people.  She is the peace keeper in times
of turmoil.  When the Queen is in play, Knights under the control of
any player may not attack her Kingdom or her Knights.  In turn, her
Knights will also refuse to attack others of the same suit.  If the
Queen is brought in to play in defense of an attack \emph{after} the
attack is declared and Knights are committed, as a penalty, the
attacking Knights may not attack or defend until the attacking
player's next Draw phase.

The Jack inspires courage amongst his Knights.  When the Jack is in
play, Knights of the same suit under the player's control receive a
bonus of $1/3$rd of the {\em maximum pip value} of the domino set.
For example:

\begin{quote}
{\em Bob has brought a set of ``double nines'' dominos, where the
maximum pip value for the set is 9.  Charlie wisely plays the Jack of
Spades during his Attack round \emph{after} his opponent, Dianne, had
committed her defenses.  All of Charlie's Spades Knights gain a bonus
of 3 to both their attack and defense powers.}
\end{quote}


The probability that a King, Queen, or Jack will come in to play is
$2/9$.

\subsection{Tens and Jokers: Wizards and Necromancers}
The two other types of cards left are the Tens and Jokers, the Wizards
and Necromancers of the deck.  The Tens, the Wizards, are masters of
disruption.  As instant effect cards, they may negate \emph{any}
special card played at any time, including other Wizards.  Some
instant cards have different periods of effectiveness.  For example,
the Power card (A$\clubsuit$) stays with the Knight until that Knight
is defeated.  The Wizard can disrupt that card.  The Death card
(A$\spadesuit$) has a very short period but deadly period of
effectiveness.  With the exception of the Wealth card
(A$\diamondsuit$), any special card that is not in the discard pile
already can be disrupted.

The Jokers are the thieving Necromancers of the deck.  They may recall
and place in a player's Recruit pool the top Knight from \emph{any}
player's graveyard stack.  This bypasses the restriction of having
only two Recruits in the Recruit pool, only so long as the resurrected
Recruit remains untrained.

\begin{itemize}
\item \textbf{Ten}: \emph{(Instant)} Wizard. Negate any special card
that has been played or is still in play.
\item \textbf{Joker}: \emph{(Instant)} Necromancer. Resurrect the top
Knight from any player's graveyard stack as a Recruit.
\end{itemize}

The probability that a Necromancer will come in to play is $1/27$.
The probability that a Wizard will come in to play is $2/27$.

\section{The Blank Domino: Doppelganger!}
The blank domino is the only special domino in a set, the
Doppelganger.  It has the ability to duplicate any Knight in play of
the same suit, including Knights under the control of opponents.  To
train the Doppelganger, the player needs only a single training card
of the same suit as the Knight she wishes to duplicate; the value of
that training card does not matter.

The Doppelganger can only duplicate one Knight until either that
Knight or the Doppelganger is defeated.  If the copied Knight is
defeated and the Doppelganger still remains in play, the player must
immediately designate another Knight of the same suit for the
Doppelganger to copy, or lose it to the graveyard.  If there are no
Knights of the same suit, the Doppelganger is immediately sent to the
graveyard.

\emph{Tip: use a marker or indicator of some kind to indicate the
copied Knight, such as a coin or piece of tape.}

Special effects cards that are applied directly to the copied Knight,
such as Life (A$\heartsuit$) or Power (A$\clubsuit$), also apply to
the Doppelganger.  Player affect cards for opponents, such as
Magesty(King), Loyalty(Queen), and Courage(Jack) do not.  Lastly, if a
special effect card is disrupted with the Wizard(Ten) on the copied
Knight, it is also disrupted on the Doppelganger.

If no Knights of the same suit are in play at the time when the
Doppelganger completes training and is promoted from Candidate to
Knight, it is immediately sent to the graveyard.

\section{Winning the Game}
The goal to winning the game is to be the ``last Kingdom standing,''
the only player with Kingdom Health Points remaining.  Some games can
take a long period of time, so setting a time limit is a practical
consideration; award the game to the ``Healthiest Kingdom after $T$
minutes.''  The goal for your game should be decided during the Setup.

\subsection{Optional Rule: Kingdom Fame Points}
\begin{quote}
{\em The rules described in this section needs some serious
play-testing to find out the behaviors of the resulting game.  Use
these rules as guidelines only and report your findings!}
\end{quote}

Rather than a game of attrition, where the survival of a Kingdom rests
upon its health, Domino Knights can be played as a type of tournament,
where skill and prowess on the field of battle is awarded with fame.

Instead of giving each Kingdom a high number of Health Points, they
are instead awarded a modest number of Fame Points, an average game
starting with 10.  For each Knight that a player's Knights defeat, a
Kingdom Fame Point is awarded.  For each player's Knight that is
defeated, she loses a Kingdom Fame Point.  If a pair of defending
Knights from different Kingdoms defeats an attacking Knight, such as
when one Kingdom is aiding another, each Kingdom receives a Fame
Point.  If you're really interested in bean-counting, award and
penalize fame points based on the pip-value of the Knight defeated.
If a player received aid in the attack, split the points accordingly.

The focus of the game is no longer attacking the player, rather
defeating opposing Knights.  Because of this change in focus, one
additional rule to consider is to allow attacking Knights to fight as
a unit, placing them on equal footing as the defending Knights.

When a player's Kingdom has no Fame Points, she is no longer able to
recruit Peasants from the countryside to fight in the Tournaments.
Knights, Candidates, and Recruits are already committed to the Kingdom
and will not abandon it, allowing the player to continue to fight with
the potential of winning battles and reclaiming Fame Points.

When a player no longer has Kingdom Fame Points, Knights, Candidates,
or Recruits, she is ejected from the game.

The goal game is also influenced in a couple ways.  A ``last Kingdom
standing'' style game can drag on forever, making it an impractical
goal.  Instead, a better goal may be ``first Kingdom with $N$ Fame
Points'' or ``the Kingdom with the most Fame points after $T$
minutes''.  An appropriate value for $N$ may be 30 or 50 points, and
an appropriate value for $T$ is probably 60 or more minutes.

\subsection{Optional Rule: Random Goal}
{\em by Gabe Turner}

Take the victory scenarios above and make them random.  At the
beginning of the game, a card is flipped over to determine the victory
scenario.  $\clubsuit$ for last player standing; $\diamondsuit$ for
first to reach $N$ Fame Points; $\heartsuit$ for most fame points
after $T$ minutes; $\spadesuit$ for last player standing with
non-increasing fame points (attrition); Jokers for whatever the group
thinks appropriate - each player allowed to argue why her favorite
scenario is the best - or a redraw.

All that remains is determining what N is, where applicable.

\section{Optional Rule: Domino-less Game}
It is still possible to play Domino Knights without a set of dominos;
the mechanics of the game change slightly.  The Peasents and Recruits
are removed from the game.  Candidates and Knights are represented by
a pair of cards of the same suit, one indicating the attack and one
indicating the defense.  Stack the defense card upon the attack card,
so that the attack values can still be seen.

As with dominos, the player does not need to assign the attack and
defense powers until the Draw phase of the next turn.

Life(A$\heartsuit$) no longer gives the player the ``no cost'' benefit
of playing it upon a domino, but a Knight may still be brought
directly into play from the player's hand.

The A$\diamondsuit$ is the card most affected by this optional rule.
One idea is to use this as a Mercenary card, allowing a player to
purchase the loyalties of any Knight on the board.  The Mercenary card
would be placed under the Knight and if disrupted, the Knight would
return to its original owner.

The Doppelganger is also generally not represented in a standard deck
of cards.  Some decks actually come with a blank card, and some come
with a card containing the rules for poker on it.  With the change to
the A$\diamondsuit$ card, it may not be necessary.

The graveyard stack mechanism also changes.  A player only needs to
keep the last Knight defeated as a pair of face-up cards, turned
horizontally (so as to distinguish it from active or inactive
Knights).  Other defeated Knights are discarded immediately.  The
graveyard stack can also be eliminated if the Necromancer card is only
allowed to act upon defeated Knights on the battle field, before the
cleanup phase of battle.

\newpage
\appendix
\section{This Document}
This document was created using \LaTeX{} and the ViM text editor.  If
you find errors in this document or have suggestions on how to improve
it, please send email to Chad C. Walstrom at
$<$\href{mailto:chewie@wookimus.net}{chewie@wookimus.net}$>$.

The latest version of this document can be found in PDF format or a
tarball of the original source code at:

\begin{quote}
\href{https://gitlab.com/runswithd6s/domino-knights}{https://gitlab.com/runswithd6s/domino-knights}
\end{quote}

\subsection{Acknowledgements}
I would like to thank my Monday night gaming group for helping play
test the initial versions of the game: Josh Sheppard, Marya Brendle,
and John Lindquist (and Mark Singer, even though he couldn't make it
that night).  I would also like to thank Gabe Turner, who proved that
it helps to have a fresh set of eyes to tackle some of the more sticky
details of editing and for bringing fresh ideas to the table.

\subsection{Changes}
\begin{itemize}
\item v0.12 - Spelling fixes. Updated to CC Attribution 4.0
International. Source moved to gitlab.
\item v0.11 - Typo in 4.3.3. example.
\item v0.10 - Optional Rule: Training Cards may be of different suit.
Cleaned up Attack description.  Removed obsolete strategy tip
regarding discard.  Fixed typos.
\item v0.9 - Renamed the Maintain phase to Promote and placed it after
the Draw phase.  Added examples.  Attack as a Unit optional rule.
Additional considerations for Kingdom Fame Points option.  Removed
ability to draw from the discard pile.  In practice, no one ever chose
this option. The longest period any Special Card can remain in play is
now one round.  Clarified benefits of Necromancer card.
\item v0.8 - Layout cleanup. Clarification of phases. Addressed change
to graveyard stack mechanism in domino-less game. Added Terminology
and Acknowledgements sections.  Added Optional Rule section for
Fluctuating Kingdom Fame Points and Gabe's Random Goal section.
\item v0.7 - Domino-less game option. (Thanks for the suggestion, Gabe!)
\item v0.6 - Copyright and License Update.
\item v0.5 - Terms Update: Change spawn and creature terminology to
Peasants, Recruits, Candidates, Knights, and Training.  Handled case
where no Knights of the same suit exist when the Doppelganger is
promoted from Candidate to Knight.
\item v0.4 - Swapped the 10's, now the disrupters, and Jokers, the
thieves.  Clarified the Wealth card and Disruption card semantics.
\item v0.3 - Added the Doppelganger!
\item v0.2 - Created spawn pool mechanism.  Require Knight card values
equal to pips.  Fixed typos.
\item v0.1 - Initial incarnation of death, destruction, and mayhem!
\end{itemize}
\end{document}
